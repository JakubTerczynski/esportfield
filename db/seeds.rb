MarkerType.delete_all
GameAnalysisMarker.delete_all
GameAnalysis.delete_all
Course.delete_all
Team.delete_all
User.delete_all
UserRole.delete_all
GameVideo.delete_all
Game.delete_all
puts 'All records deleted'


# Games
games = Game.create!([
  { name: 'Counter Strike' },
  { name: 'DOTA 2' },
  { name: 'Fortnite' },
  { name: 'Smerfy' }
])
puts 'Games created'

# UserRoles
user_roles = UserRole.create!([
  { name: 'app_admin', level: 0 },
  { name: 'founder', level: 1 },
  { name: 'coach', level: 2 },
  { name: 'player', level: 3 }
])
puts 'User Roles created'

# Users
users = User.create!([
  { email: 'admin@esportfield.com', nickname: 'Admin', user_role: user_roles[0], game: games[0], password: 'admintest', password_confirmation: 'admintest' },
  { email: 'founder@esportfield.com', nickname: 'Founder', user_role: user_roles[1], game: games[0], password: 'foundertest', password_confirmation: 'foundertest' },
  { email: 'coach@esportfield.com', nickname: 'Coach', user_role: user_roles[2], game: games[0], password: 'coachtest', password_confirmation: 'coachtest' },
  { email: 'player@esportfield.com', nickname: 'Player', user_role: user_roles[3], game: games[0], password: 'playertest', password_confirmation: 'playertest' }
])
puts 'Users created'

# Teams
teams = Team.create!([
  { name: 'Yellow', founder: users[1], game: games[0] },
  { name: 'Green', founder: users[1], game: games[0] },
  { name: 'Alfa', founder: users[1], game: games[0] },
  { name: 'Bravo', founder: users[1], game: games[0] }
])
puts 'Teams created'

# Courses
courses = Course.create!([
  { name: 'Begginer course', coach: users[2], game: games[0] },
  { name: 'Intermediate course', coach: users[2], game: games[0] },
  { name: 'Pro course', coach: users[2], game: games[0] },
  { name: 'God mode course', coach: users[2], game: games[0] }
])
puts 'Courses created'

# Marker Types
marker_types = MarkerType.create!([
  { name: 'comment', color: '00FF00' },
  { name: 'moving', color: 'FF0000' },
  { name: 'shooting', color: '0000FF' }
])
puts 'Marker Types created'

# Game Analyses
# game_analyses = GameAnalysis.create!([
#   { game_video_id: 2, summary: 'trolololo', recommendation: 'lalalala', coach: users[2]}
# ])
# puts 'Game Analyses created'

# Game Analysis Markers
# game_analysis_markers = GameAnalysisMarker.create!([
#   { game_analysis: game_analyses[0], frame_from: 20, frame_to: 120, comment: 'trololo', marker_type: marker_types[0], image: 'img'}
#   ])
# puts 'Game Analysis Markers created'
