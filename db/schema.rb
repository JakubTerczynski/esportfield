# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_09_09_085533) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "courses", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "coach_id"
    t.bigint "game_id"
    t.index ["game_id"], name: "index_courses_on_game_id"
  end

  create_table "game_analyses", force: :cascade do |t|
    t.bigint "game_video_id"
    t.string "summary"
    t.string "recommendation"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "coach_id"
    t.index ["game_video_id"], name: "index_game_analyses_on_game_video_id"
  end

  create_table "game_analysis_markers", force: :cascade do |t|
    t.bigint "game_analysis_id"
    t.integer "frame_from"
    t.integer "frame_to"
    t.string "comment"
    t.bigint "marker_type_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image"
    t.index ["game_analysis_id"], name: "index_game_analysis_markers_on_game_analysis_id"
    t.index ["marker_type_id"], name: "index_game_analysis_markers_on_marker_type_id"
  end

  create_table "game_videos", force: :cascade do |t|
    t.bigint "game_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "video"
    t.integer "player_id"
    t.index ["game_id"], name: "index_game_videos_on_game_id"
  end

  create_table "games", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "marker_types", force: :cascade do |t|
    t.string "name"
    t.string "color"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "post_contents", force: :cascade do |t|
    t.string "text_content"
    t.string "subtitle_content"
    t.string "image_content"
    t.bigint "post_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["post_id"], name: "index_post_contents_on_post_id"
  end

  create_table "posts", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "tag"
    t.string "image"
  end

  create_table "teams", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "founder_id"
    t.bigint "game_id"
    t.string "logo"
    t.index ["game_id"], name: "index_teams_on_game_id"
  end

  create_table "user_roles", force: :cascade do |t|
    t.string "name"
    t.integer "level"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "nickname"
    t.string "authentication_token", limit: 30
    t.bigint "user_role_id"
    t.bigint "game_id"
    t.string "avatar"
    t.index ["authentication_token"], name: "index_users_on_authentication_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["game_id"], name: "index_users_on_game_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["user_role_id"], name: "index_users_on_user_role_id"
  end

  add_foreign_key "courses", "games"
  add_foreign_key "courses", "users", column: "coach_id"
  add_foreign_key "game_analyses", "game_videos"
  add_foreign_key "game_analyses", "users", column: "coach_id"
  add_foreign_key "game_analysis_markers", "game_analyses"
  add_foreign_key "game_analysis_markers", "marker_types"
  add_foreign_key "game_videos", "games"
  add_foreign_key "game_videos", "users", column: "player_id"
  add_foreign_key "post_contents", "posts"
  add_foreign_key "teams", "games"
  add_foreign_key "teams", "users", column: "founder_id"
  add_foreign_key "users", "games"
  add_foreign_key "users", "user_roles"
end
