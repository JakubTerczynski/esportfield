class AddFounderToTeams < ActiveRecord::Migration[5.2]
  def change
    add_column :teams, :founder_id, :integer

    add_foreign_key :teams, :users, column: :founder_id
  end
end
