class AddPlayerToGameVideos < ActiveRecord::Migration[5.2]
  def change
    add_column :game_videos, :player_id, :integer

    add_foreign_key :game_videos, :users, column: :player_id
  end
end
