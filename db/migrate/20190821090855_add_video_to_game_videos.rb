class AddVideoToGameVideos < ActiveRecord::Migration[5.2]
  def change
    add_column :game_videos, :video, :string
  end
end
