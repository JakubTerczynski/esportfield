class RemoveLinkFromGameAnalyses < ActiveRecord::Migration[5.2]
  def change
    remove_column :game_analyses, :link, :string
  end
end
