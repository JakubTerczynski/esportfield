class CreateGameVideos < ActiveRecord::Migration[5.2]
  def change
    create_table :game_videos do |t|
      t.references :game, foreign_key: true
      t.string :link

      t.timestamps
    end
  end
end
