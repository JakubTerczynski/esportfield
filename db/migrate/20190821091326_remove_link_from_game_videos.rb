class RemoveLinkFromGameVideos < ActiveRecord::Migration[5.2]
  def change
    remove_column :game_videos, :link, :string
  end
end
