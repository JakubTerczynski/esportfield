class AddImageToGameAnalysisMarkers < ActiveRecord::Migration[5.2]
  def change
    add_column :game_analysis_markers, :image, :string
  end
end
