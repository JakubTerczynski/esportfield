class CreateGameAnalyses < ActiveRecord::Migration[5.2]
  def change
    create_table :game_analyses do |t|
      t.references :game_video, foreign_key: true
      t.string :link
      t.string :summary
      t.string :recommendation

      t.timestamps
    end
  end
end
