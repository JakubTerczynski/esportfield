class AddCoachToCourses < ActiveRecord::Migration[5.2]
  def change
    add_column :courses, :coach_id, :integer

    add_foreign_key :courses, :users, column: :coach_id
  end
end
