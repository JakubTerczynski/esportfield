class AddCoachToGameAnalyses < ActiveRecord::Migration[5.2]
  def change
    add_column :game_analyses, :coach_id, :integer

    add_foreign_key :game_analyses, :users, column: :coach_id
  end
end
