class CreatePostContents < ActiveRecord::Migration[5.2]
  def change
    create_table :post_contents do |t|
      t.string :text_content
      t.string :subtitle_content
      t.string :image_content
      t.belongs_to :post, foreign_key: true

      t.timestamps
    end
  end
end
