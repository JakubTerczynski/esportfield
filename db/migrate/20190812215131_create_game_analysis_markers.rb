class CreateGameAnalysisMarkers < ActiveRecord::Migration[5.2]
  def change
    create_table :game_analysis_markers do |t|
      t.references :game_analysis, foreign_key: true
      t.integer :frame_from
      t.integer :frame_to
      t.string :comment
      t.references :marker_type, foreign_key: true

      t.timestamps
    end
  end
end
