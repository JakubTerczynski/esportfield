Rails.application.routes.draw do
  resources :marker_types
  resources :game_analysis_markers
  resources :game_analyses
  resources :game_videos
  resources :games
  resources :teams
  resources :courses
  resources :user_roles
  resources :post_contents
  resources :posts
  resources :pages, only: [:show]

  namespace :users do
    resources :custom, except: [:create, :update]
    # post "custom/create", to: "custom#create", as: "create_custom"
    # patch "custom/update/:id", to: "custom#update", as: "update_custom"
    # post "custom/putrole/:id", to: "custom#put_role", as: "put_role_custom"
  end

  get 'users', to: 'users/custom#index', as: 'users'
  get 'about', to: 'pages#about', as: 'about'
  get 'dashboard', to: 'pages#dashboard', as: 'dashboard'
  get 'contact', to: 'pages#contact', as: 'contact'
  get 'blog', to: redirect('http://esportfield.com/'), as: 'blog'
  post "subscribe", to: "pages#subscribe", as: "subscribe"
  post "send_message", to: "pages#send_message", as: "send_message"

  devise_for :users, controllers: {
    sessions: 'users/sessions', registrations: 'users/registrations', confirmations: 'users/confirmations'
  }

  # API
  namespace :api, defaults: { format: 'json' } do
    namespace :v1 do
      devise_for :users, controllers: { sessions: 'api/v1/users/sessions' }
      namespace :users do
        resource :sessions, only: [:create, :destroy, :show]
        resources :custom, only: [:index, :putrole, :create, :update, :destroy, :show]
        get 'founders', to: 'custom#index_founders', as: 'users_founders'
        get 'coaches', to: 'custom#index_coaches', as: 'users_coaches'
        get 'players', to: 'custom#index_players', as: 'users_players'
      end
      get 'dashboard', to: 'pages#dashboard', as: 'dashboard'
      resources :courses, only: [:index, :create, :update, :destroy, :show]
      resources :teams, only: [:index, :create, :update, :destroy, :show]
      resources :user_roles, only: [:index, :create, :update, :destroy, :show]
      resources :marker_types, only: [:index, :create, :update, :destroy, :show]
      resources :games, only: [:index, :create, :update, :destroy, :show]
      resources :game_videos, only: [:index, :create, :update, :destroy, :show]
      resources :game_analyses, only: [:index, :create, :update, :destroy, :show]
      get 'game_analyses/index_by_coach/:coach_id', to: 'game_analyses#index_by_coach', as: 'index_by_coach'
      resources :game_analysis_markers, only: [:index, :create, :update, :destroy, :show]
      get 'game_analysis_markers/index_by_game_analysis/:game_analysis_id', to: 'game_analysis_markers#index_by_game_analysis', as: 'index_by_game_analysis'
    end
  end

  root to: 'pages#about'
end
