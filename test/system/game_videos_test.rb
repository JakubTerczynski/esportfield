require "application_system_test_case"

class GameVideosTest < ApplicationSystemTestCase
  setup do
    @game_video = game_videos(:one)
  end

  test "visiting the index" do
    visit game_videos_url
    assert_selector "h1", text: "Game Videos"
  end

  test "creating a Game video" do
    visit game_videos_url
    click_on "New Game Video"

    fill_in "Game", with: @game_video.game
    fill_in "Link", with: @game_video.link
    click_on "Create Game video"

    assert_text "Game video was successfully created"
    click_on "Back"
  end

  test "updating a Game video" do
    visit game_videos_url
    click_on "Edit", match: :first

    fill_in "Game", with: @game_video.game
    fill_in "Link", with: @game_video.link
    click_on "Update Game video"

    assert_text "Game video was successfully updated"
    click_on "Back"
  end

  test "destroying a Game video" do
    visit game_videos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Game video was successfully destroyed"
  end
end
