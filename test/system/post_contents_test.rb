require "application_system_test_case"

class PostContentsTest < ApplicationSystemTestCase
  setup do
    @post_content = post_contents(:one)
  end

  test "visiting the index" do
    visit post_contents_url
    assert_selector "h1", text: "Post Contents"
  end

  test "creating a Post content" do
    visit post_contents_url
    click_on "New Post Content"

    fill_in "Image content", with: @post_content.image_content
    fill_in "Post", with: @post_content.post_id
    fill_in "Subtitle content", with: @post_content.subtitle_content
    fill_in "Text content", with: @post_content.text_content
    click_on "Create Post content"

    assert_text "Post content was successfully created"
    click_on "Back"
  end

  test "updating a Post content" do
    visit post_contents_url
    click_on "Edit", match: :first

    fill_in "Image content", with: @post_content.image_content
    fill_in "Post", with: @post_content.post_id
    fill_in "Subtitle content", with: @post_content.subtitle_content
    fill_in "Text content", with: @post_content.text_content
    click_on "Update Post content"

    assert_text "Post content was successfully updated"
    click_on "Back"
  end

  test "destroying a Post content" do
    visit post_contents_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Post content was successfully destroyed"
  end
end
