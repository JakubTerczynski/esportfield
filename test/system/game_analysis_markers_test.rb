require "application_system_test_case"

class GameAnalysisMarkersTest < ApplicationSystemTestCase
  setup do
    @game_analysis_marker = game_analysis_markers(:one)
  end

  test "visiting the index" do
    visit game_analysis_markers_url
    assert_selector "h1", text: "Game Analysis Markers"
  end

  test "creating a Game analysis marker" do
    visit game_analysis_markers_url
    click_on "New Game Analysis Marker"

    fill_in "Comment", with: @game_analysis_marker.comment
    fill_in "Frame from", with: @game_analysis_marker.frame_from
    fill_in "Frame to", with: @game_analysis_marker.frame_to
    fill_in "Game analysis", with: @game_analysis_marker.game_analysis
    fill_in "Marker type", with: @game_analysis_marker.marker_type
    click_on "Create Game analysis marker"

    assert_text "Game analysis marker was successfully created"
    click_on "Back"
  end

  test "updating a Game analysis marker" do
    visit game_analysis_markers_url
    click_on "Edit", match: :first

    fill_in "Comment", with: @game_analysis_marker.comment
    fill_in "Frame from", with: @game_analysis_marker.frame_from
    fill_in "Frame to", with: @game_analysis_marker.frame_to
    fill_in "Game analysis", with: @game_analysis_marker.game_analysis
    fill_in "Marker type", with: @game_analysis_marker.marker_type
    click_on "Update Game analysis marker"

    assert_text "Game analysis marker was successfully updated"
    click_on "Back"
  end

  test "destroying a Game analysis marker" do
    visit game_analysis_markers_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Game analysis marker was successfully destroyed"
  end
end
