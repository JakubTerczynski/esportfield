require "application_system_test_case"

class GameAnalysesTest < ApplicationSystemTestCase
  setup do
    @game_analysis = game_analyses(:one)
  end

  test "visiting the index" do
    visit game_analyses_url
    assert_selector "h1", text: "Game Analyses"
  end

  test "creating a Game analysis" do
    visit game_analyses_url
    click_on "New Game Analysis"

    fill_in "Game video", with: @game_analysis.game_video
    fill_in "Link", with: @game_analysis.link
    fill_in "Recommendation", with: @game_analysis.recommendation
    fill_in "Summary", with: @game_analysis.summary
    click_on "Create Game analysis"

    assert_text "Game analysis was successfully created"
    click_on "Back"
  end

  test "updating a Game analysis" do
    visit game_analyses_url
    click_on "Edit", match: :first

    fill_in "Game video", with: @game_analysis.game_video
    fill_in "Link", with: @game_analysis.link
    fill_in "Recommendation", with: @game_analysis.recommendation
    fill_in "Summary", with: @game_analysis.summary
    click_on "Update Game analysis"

    assert_text "Game analysis was successfully updated"
    click_on "Back"
  end

  test "destroying a Game analysis" do
    visit game_analyses_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Game analysis was successfully destroyed"
  end
end
