require "application_system_test_case"

class MarkerTypesTest < ApplicationSystemTestCase
  setup do
    @marker_type = marker_types(:one)
  end

  test "visiting the index" do
    visit marker_types_url
    assert_selector "h1", text: "Marker Types"
  end

  test "creating a Marker type" do
    visit marker_types_url
    click_on "New Marker Type"

    fill_in "Color", with: @marker_type.color
    fill_in "Name", with: @marker_type.name
    click_on "Create Marker type"

    assert_text "Marker type was successfully created"
    click_on "Back"
  end

  test "updating a Marker type" do
    visit marker_types_url
    click_on "Edit", match: :first

    fill_in "Color", with: @marker_type.color
    fill_in "Name", with: @marker_type.name
    click_on "Update Marker type"

    assert_text "Marker type was successfully updated"
    click_on "Back"
  end

  test "destroying a Marker type" do
    visit marker_types_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Marker type was successfully destroyed"
  end
end
