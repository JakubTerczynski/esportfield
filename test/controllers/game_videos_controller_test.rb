require 'test_helper'

class GameVideosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @game_video = game_videos(:one)
  end

  test "should get index" do
    get game_videos_url
    assert_response :success
  end

  test "should get new" do
    get new_game_video_url
    assert_response :success
  end

  test "should create game_video" do
    assert_difference('GameVideo.count') do
      post game_videos_url, params: { game_video: { game: @game_video.game, link: @game_video.link } }
    end

    assert_redirected_to game_video_url(GameVideo.last)
  end

  test "should show game_video" do
    get game_video_url(@game_video)
    assert_response :success
  end

  test "should get edit" do
    get edit_game_video_url(@game_video)
    assert_response :success
  end

  test "should update game_video" do
    patch game_video_url(@game_video), params: { game_video: { game: @game_video.game, link: @game_video.link } }
    assert_redirected_to game_video_url(@game_video)
  end

  test "should destroy game_video" do
    assert_difference('GameVideo.count', -1) do
      delete game_video_url(@game_video)
    end

    assert_redirected_to game_videos_url
  end
end
