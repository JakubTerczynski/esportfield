require 'test_helper'

class GameAnalysisMarkersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @game_analysis_marker = game_analysis_markers(:one)
  end

  test "should get index" do
    get game_analysis_markers_url
    assert_response :success
  end

  test "should get new" do
    get new_game_analysis_marker_url
    assert_response :success
  end

  test "should create game_analysis_marker" do
    assert_difference('GameAnalysisMarker.count') do
      post game_analysis_markers_url, params: { game_analysis_marker: { comment: @game_analysis_marker.comment, frame_from: @game_analysis_marker.frame_from, frame_to: @game_analysis_marker.frame_to, game_analysis: @game_analysis_marker.game_analysis, marker_type: @game_analysis_marker.marker_type } }
    end

    assert_redirected_to game_analysis_marker_url(GameAnalysisMarker.last)
  end

  test "should show game_analysis_marker" do
    get game_analysis_marker_url(@game_analysis_marker)
    assert_response :success
  end

  test "should get edit" do
    get edit_game_analysis_marker_url(@game_analysis_marker)
    assert_response :success
  end

  test "should update game_analysis_marker" do
    patch game_analysis_marker_url(@game_analysis_marker), params: { game_analysis_marker: { comment: @game_analysis_marker.comment, frame_from: @game_analysis_marker.frame_from, frame_to: @game_analysis_marker.frame_to, game_analysis: @game_analysis_marker.game_analysis, marker_type: @game_analysis_marker.marker_type } }
    assert_redirected_to game_analysis_marker_url(@game_analysis_marker)
  end

  test "should destroy game_analysis_marker" do
    assert_difference('GameAnalysisMarker.count', -1) do
      delete game_analysis_marker_url(@game_analysis_marker)
    end

    assert_redirected_to game_analysis_markers_url
  end
end
