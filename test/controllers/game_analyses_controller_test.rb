require 'test_helper'

class GameAnalysesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @game_analysis = game_analyses(:one)
  end

  test "should get index" do
    get game_analyses_url
    assert_response :success
  end

  test "should get new" do
    get new_game_analysis_url
    assert_response :success
  end

  test "should create game_analysis" do
    assert_difference('GameAnalysis.count') do
      post game_analyses_url, params: { game_analysis: { game_video: @game_analysis.game_video, link: @game_analysis.link, recommendation: @game_analysis.recommendation, summary: @game_analysis.summary } }
    end

    assert_redirected_to game_analysis_url(GameAnalysis.last)
  end

  test "should show game_analysis" do
    get game_analysis_url(@game_analysis)
    assert_response :success
  end

  test "should get edit" do
    get edit_game_analysis_url(@game_analysis)
    assert_response :success
  end

  test "should update game_analysis" do
    patch game_analysis_url(@game_analysis), params: { game_analysis: { game_video: @game_analysis.game_video, link: @game_analysis.link, recommendation: @game_analysis.recommendation, summary: @game_analysis.summary } }
    assert_redirected_to game_analysis_url(@game_analysis)
  end

  test "should destroy game_analysis" do
    assert_difference('GameAnalysis.count', -1) do
      delete game_analysis_url(@game_analysis)
    end

    assert_redirected_to game_analyses_url
  end
end
