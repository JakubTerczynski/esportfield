require 'test_helper'

class MarkerTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @marker_type = marker_types(:one)
  end

  test "should get index" do
    get marker_types_url
    assert_response :success
  end

  test "should get new" do
    get new_marker_type_url
    assert_response :success
  end

  test "should create marker_type" do
    assert_difference('MarkerType.count') do
      post marker_types_url, params: { marker_type: { color: @marker_type.color, name: @marker_type.name } }
    end

    assert_redirected_to marker_type_url(MarkerType.last)
  end

  test "should show marker_type" do
    get marker_type_url(@marker_type)
    assert_response :success
  end

  test "should get edit" do
    get edit_marker_type_url(@marker_type)
    assert_response :success
  end

  test "should update marker_type" do
    patch marker_type_url(@marker_type), params: { marker_type: { color: @marker_type.color, name: @marker_type.name } }
    assert_redirected_to marker_type_url(@marker_type)
  end

  test "should destroy marker_type" do
    assert_difference('MarkerType.count', -1) do
      delete marker_type_url(@marker_type)
    end

    assert_redirected_to marker_types_url
  end
end
