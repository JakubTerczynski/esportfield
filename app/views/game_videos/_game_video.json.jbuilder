json.extract! game_video, :id, :game, :video, :created_at, :updated_at
json.url game_video_url(game_video, format: :json)
