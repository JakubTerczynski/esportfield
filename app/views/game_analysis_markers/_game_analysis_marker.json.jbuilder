json.extract! game_analysis_marker, :id, :game_analysis, :frame_from, :frame_to, :comment, :marker_type, :image, :created_at, :updated_at
json.url game_analysis_marker_url(game_analysis_marker, format: :json)
