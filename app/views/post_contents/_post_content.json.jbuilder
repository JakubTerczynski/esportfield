json.extract! post_content, :id, :text_content, :subtitle_content, :image_content, :post_id, :created_at, :updated_at
json.url post_content_url(post_content, format: :json)
