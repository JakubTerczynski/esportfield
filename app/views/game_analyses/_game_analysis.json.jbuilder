json.extract! game_analysis, :id, :game_video, :link, :summary, :recommendation, :created_at, :updated_at
json.url game_analysis_url(game_analysis, format: :json)
