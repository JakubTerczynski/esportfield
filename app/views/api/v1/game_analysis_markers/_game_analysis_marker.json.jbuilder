json.extract! game_analysis_marker, :id, :game_analysis, :frame_from, :frame_to, :comment, :image, :created_at, :updated_at
json.marker_type do
  if game_analysis_marker.marker_type.present?
    json.extract! game_analysis_marker.marker_type, :id, :name, :color
  else
    json.null!
  end
end
json.url game_analysis_marker_url(game_analysis_marker, format: :json)
