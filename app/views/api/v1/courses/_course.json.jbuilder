json.extract! course, :id, :name, :created_at, :updated_at
json.coach do
  json.extract! course.coach, :id, :email, :nickname
end
json.url course_url(course, format: :json)
