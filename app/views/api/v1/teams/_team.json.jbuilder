json.extract! team, :id, :name, :logo, :created_at, :updated_at
json.founder do
  json.extract! team.founder, :id, :email, :nickname
end
json.game do
  json.extract! team.game, :id, :name
end
json.url team_url(team, format: :json)
