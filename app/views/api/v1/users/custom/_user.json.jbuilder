json.extract! user, :id, :email, :nickname, :avatar
json.user_role do
  json.extract! user.user_role, :id, :name, :level
end
json.game do
  json.extract! user.game, :id, :name
end
