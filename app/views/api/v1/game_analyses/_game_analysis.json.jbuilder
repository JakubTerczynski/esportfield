json.extract! game_analysis, :id, :game_video_id, :summary, :recommendation, :created_at, :updated_at
json.game_video do
  json.extract! game_analysis.game_video, :id, :video, :created_at, :updated_at
end
json.coach do
  json.extract! game_analysis.coach, :id, :email, :nickname
end
json.url game_analysis_url(game_analysis, format: :json)
