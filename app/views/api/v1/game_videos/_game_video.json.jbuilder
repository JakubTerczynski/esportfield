json.extract! game_video, :id, :video, :created_at, :updated_at
json.game do
  json.extract! game_video.game, :id, :name
end
json.player do
  json.extract! game_video.player, :id, :email, :nickname
end
json.url game_video_url(game_video, format: :json)
