json.extract! marker_type, :id, :name, :color, :created_at, :updated_at
json.url marker_type_url(marker_type, format: :json)
