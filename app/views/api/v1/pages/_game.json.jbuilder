json.extract! game, :id, :name, :logo, :created_at, :updated_at
json.teams do
  json.array! game.teams.limit(4), partial: 'api/v1/pages/team', as: :team
end
json.players do
  json.array! game.users.includes(:user_role).where(user_roles: {level: 3}).limit(4), partial: 'api/v1/pages/player', as: :player
end
json.coaches do
  json.array! game.users.includes(:user_role).where(user_roles: {level: 2}).limit(4), partial: 'api/v1/pages/coach', as: :coach
end
json.courses do
  json.array! game.courses.limit(4), partial: 'api/v1/pages/course', as: :course
end
