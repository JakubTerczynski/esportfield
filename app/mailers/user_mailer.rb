class UserMailer < ApplicationMailer
  default from: "esportfield1@gmail.com"

  def signup_confirmation(user)
    @user = user
    mail to: user.email, subject: "Sign Up Confirmation"
  end

  # def contact_message(user_message)
  #   @user_message = user_message
  #   mail to: user.email, subject: "Sign Up Confirmation"
  # end
end
