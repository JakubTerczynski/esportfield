class ApplicationController < ActionController::Base
  protect_from_forgery prepend: true

  acts_as_token_authentication_handler_for User, fallback: :none
end
