class Api::V1::PagesController < ApplicationController
  respond_to :json

  def dashboard
    @games = Game.includes(:users, :courses, :teams)
    respond_with @games
  end

  private
		# Never trust parameters from the scary internet, only allow the white list through.
		def user_params
			params.require(:user).permit(:nickname, :email, :password)
		end
		def contact_form_params
			params.require(:contact_form).permit(:nickname, :email, :name, :message)
		end
end
