class Api::V1::GameAnalysisMarkersController < ApplicationController
  before_action :set_game_analysis_marker, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  respond_to :json

  # GET /game_analysis_markers
  # GET /game_analysis_markers.json
  def index
    @game_analysis_markers = GameAnalysisMarker.all
    respond_with @game_analysis_markers
  end

  def index_by_game_analysis
    @game_analysis_markers = GameAnalysisMarker.where(game_analysis_id: params[:game_analysis_id])
    respond_with @game_analysis_markers
  end

  # GET /game_analysis_markers/1
  # GET /game_analysis_markers/1.json
  def show
    respond_with @game_analysis_marker
  end

  # POST /game_analysis_markers
  # POST /game_analysis_markers.json
  def create
    @game_analysis_marker = GameAnalysisMarker.create(game_analysis_marker_params)
    respond_with @game_analysis_marker
  end

  # PATCH/PUT /game_analysis_markers/1
  # PATCH/PUT /game_analysis_markers/1.json
  def update
    @game_analysis_marker.update(game_analysis_marker_params)
    respond_with @game_analysis_marker
    # else
    #   respond_with @game_analysis_marker.errors, status: :unprocessable_entity
    # end
  end

  # DELETE /game_analysis_markers/1
  # DELETE /game_analysis_markers/1.json
  def destroy
    respond_with @game_analysis_marker.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_game_analysis_marker
      @game_analysis_marker = GameAnalysisMarker.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def game_analysis_marker_params
      params.require(:game_analysis_marker).permit(:game_analysis_id, :frame_from, :frame_to, :comment, :marker_type_id, :image)
    end
end
