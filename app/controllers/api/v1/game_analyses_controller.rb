class Api::V1::GameAnalysesController < ApplicationController
  before_action :set_game_analysis, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  respond_to :json

  # GET /game_analyses
  # GET /game_analyses.json
  def index
    @game_analyses = GameAnalysis.all
    respond_with @game_analyses
  end

  def index_by_coach
    @game_analyses = GameAnalysis.where(coach_id: params[:coach_id])
    respond_with @game_analyses
  end

  # GET /game_analyses/1
  # GET /game_analyses/1.json
  def show
    respond_with @game_analysis
  end

  # POST /game_analyses
  # POST /game_analyses.json
  def create
    @game_analysis = GameAnalysis.create(game_analysis_params)
    respond_with @game_analysis
  end

  # PATCH/PUT /game_analyses/1
  # PATCH/PUT /game_analyses/1.json
  def update
    respond_with @game_analysis.update(game_analysis_params)
  end

  # DELETE /game_analyses/1
  # DELETE /game_analyses/1.json
  def destroy
    respond_with @game_analysis.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_game_analysis
      @game_analysis = GameAnalysis.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def game_analysis_params
      params.require(:game_analysis).permit(:game_video_id, :summary, :recommendation, :coach_id)
    end
end
