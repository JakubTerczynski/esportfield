class Api::V1::GameVideosController < ApplicationController
  before_action :set_game_video, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  respond_to :json

  # GET /game_videos
  # GET /game_videos.json
  def index
    @game_videos = GameVideo.all
    respond_with @game_videos
  end

  # GET /game_videos/1
  # GET /game_videos/1.json
  def show
    respond_with @game_video
  end

  # POST /game_videos
  # POST /game_videos.json
  def create
    @game_video = GameVideo.create(game_video_params)
    respond_with @game_video
  end

  # PATCH/PUT /game_videos/1
  # PATCH/PUT /game_videos/1.json
  def update
    respond_with @game_video.update(game_video_params)
  end

  # DELETE /game_videos/1
  # DELETE /game_videos/1.json
  def destroy
    respond_with @game_video.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_game_video
      @game_video = GameVideo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def game_video_params
      params.require(:game_video).permit(:game_id, :video, :player_id)
    end
end
