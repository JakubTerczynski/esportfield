class Api::V1::CoursesController < ApplicationController
  before_action :set_course, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  respond_to :json

  # GET /courses
  # GET /courses.json
  def index
    @courses = Course.all
    respond_with @courses
  end

  # GET /courses/1
  # GET /courses/1.json
  def show
    respond_with @course
  end

  # POST /courses
  # POST /courses.json
  def create
    @course = Course.create(course_params)
    respond_with @course
  end

  # PATCH/PUT /courses/1
  # PATCH/PUT /courses/1.json
  def update
    respond_with @course.update(course_params)
  end

  # DELETE /courses/1
  # DELETE /courses/1.json
  def destroy
    respond_with @course.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_course
      @course = Course.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def course_params
      params.require(:course).permit(:name, :coach_id)
    end
end
