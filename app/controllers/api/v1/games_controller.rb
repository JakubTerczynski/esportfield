class Api::V1::GamesController < ApplicationController
  before_action :set_game, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:create, :edit, :update, :destroy]
  respond_to :json

  # GET /games
  # GET /games.json
  def index
    @games = Game.all
    respond_with @games
  end

  # GET /games/1
  # GET /games/1.json
  def show
    respond_with @game
  end

  # POST /games
  # POST /games.json
  def create
    @game = Game.create(game_params)
    respond_with @game
  end

  # PATCH/PUT /games/1
  # PATCH/PUT /games/1.json
  def update
    respond_with @game.update(game_params)
  end

  # DELETE /games/1
  # DELETE /games/1.json
  def destroy
    respond_with @game.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_game
      @game = Game.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def game_params
      params.require(:game).permit(:name, :logo)
    end
end
