class Api::V1::UserRolesController < ApplicationController
  before_action :set_user_role, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:create, :edit, :update, :destroy]
	respond_to :json

  # GET /user_roles
  # GET /user_roles.json
  def index
    @user_roles = UserRole.all
    respond_with @user_roles
  end

  # GET /user_roles/1
  # GET /user_roles/1.json
  def show
    respond_with @user_role
  end

  # POST /user_roles
  # POST /user_roles.json
  def create
    @user_role = UserRole.create(user_role_params)
    respond_with @user_role
  end

  # PATCH/PUT /user_roles/1
  # PATCH/PUT /user_roles/1.json
  def update
    respond_with @user_role.update(user_role_params)
  end

  # DELETE /user_roles/1
  # DELETE /user_roles/1.json
  def destroy
    respond_with @user_role.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_role
      @user_role = UserRole.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_role_params
      params.require(:user_role).permit(:name, :level)
    end
end
