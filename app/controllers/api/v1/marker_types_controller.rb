class Api::V1::MarkerTypesController < ApplicationController
  before_action :set_marker_type, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  respond_to :json

  # GET /marker_types
  # GET /marker_types.json
  def index
    @marker_types = MarkerType.all
    respond_with @marker_types
  end

  # GET /marker_types/1
  # GET /marker_types/1.json
  def show
    respond_with @marker_type
  end

  # POST /marker_types
  # POST /marker_types.json
  def create
    @marker_type = MarkerType.create(marker_type_params)
    respond_with @marker_type
  end

  # PATCH/PUT /marker_types/1
  # PATCH/PUT /marker_types/1.json
  def update
    respond_with @marker_type.update(marker_type_params)
  end

  # DELETE /marker_types/1
  # DELETE /marker_types/1.json
  def destroy
    respond_with @marker_type.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_marker_type
      @marker_type = MarkerType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def marker_type_params
      params.require(:marker_type).permit(:name, :color)
    end
end
