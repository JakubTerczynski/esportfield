class Api::V1::TeamsController < ApplicationController
  before_action :set_team, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:create, :edit, :update, :destroy]
	respond_to :json

  # GET /teams
  # GET /teams.json
  def index
    @teams = Team.all
    respond_with @teams
  end

  # GET /teams/1
  # GET /teams/1.json
  def show
    respond_with @team
  end

  # POST /teams
  # POST /teams.json
  def create
    @team = Team.create(team_params)
    respond_with @team
  end

  # PATCH/PUT /teams/1
  # PATCH/PUT /teams/1.json
  def update
    respond_with @team.update(team_params)
  end

  # DELETE /teams/1
  # DELETE /teams/1.json
  def destroy
    respond_with @team.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_team
      @team = Team.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def team_params
      params.require(:team).permit(:name, :founder_id, :game_id, :logo)
    end
end
