class Api::V1::Users::CustomController < ApplicationController
	before_action :set_user, only: [:show, :update, :destroy, :put_role]
  before_action :authenticate_user!, only: [:edit, :update, :destroy]
	respond_to :json

	def index
		@users = User.all
		respond_with @users
	end

	def index_founders
		@users = User.includes(:user_role).where(user_roles: {level: 1})
		respond_with @users
	end

	def index_coaches
		@users = User.includes(:user_role).where(user_roles: {level: 2})
		respond_with @users
	end

	def index_players
		@users = User.includes(:user_role).where(user_roles: {level: 3})
		respond_with @users
	end

	def show
	  respond_with @user
	end

	def put_role
		# @user_role = UserRole.where(id: params[:user_role_id]).first
		# respond_to do |format|
		# 	if @user.update({user_role_id: @user_role.id})
		# 		format.html { redirect_to users_custom_index_path, notice: 'Rola użytkownika została edytowana pomyślnie.' }
		# 	end
		# end
	end

	def create
		@user = User.create(user_params)
		respond_with @user
	end

	# PATCH/PUT /users/1
	# PATCH/PUT /users/1.json
	def update
		if current_user.is_admin? || current_user.id == @user.id
			respond_with @user.update(user_params)
		end
	end

	# DELETE /users/1
	# DELETE /users/1.json
	def destroy
		respond_with @user.destroy
	end

	private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
			@user = User.find(params[:id])
    end

		# Never trust parameters from the scary internet, only allow the white list through.
		def user_params
			params.require(:user).permit(:email, :password, :nickname, :user_role_id, :game_id, :avatar)
		end

end
