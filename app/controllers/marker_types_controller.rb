class MarkerTypesController < ApplicationController
  before_action :set_marker_type, only: [:show, :edit, :update, :destroy]

  # GET /marker_types
  # GET /marker_types.json
  def index
    @marker_types = MarkerType.all
  end

  # GET /marker_types/1
  # GET /marker_types/1.json
  def show
  end

  # GET /marker_types/new
  def new
    @marker_type = MarkerType.new
  end

  # GET /marker_types/1/edit
  def edit
  end

  # POST /marker_types
  # POST /marker_types.json
  def create
    @marker_type = MarkerType.new(marker_type_params)

    respond_to do |format|
      if @marker_type.save
        format.html { redirect_to @marker_type, notice: 'Marker type was successfully created.' }
        format.json { render :show, status: :created, location: @marker_type }
      else
        format.html { render :new }
        format.json { render json: @marker_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /marker_types/1
  # PATCH/PUT /marker_types/1.json
  def update
    respond_to do |format|
      if @marker_type.update(marker_type_params)
        format.html { redirect_to @marker_type, notice: 'Marker type was successfully updated.' }
        format.json { render :show, status: :ok, location: @marker_type }
      else
        format.html { render :edit }
        format.json { render json: @marker_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /marker_types/1
  # DELETE /marker_types/1.json
  def destroy
    @marker_type.destroy
    respond_to do |format|
      format.html { redirect_to marker_types_url, notice: 'Marker type was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_marker_type
      @marker_type = MarkerType.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def marker_type_params
      params.require(:marker_type).permit(:name, :color)
    end
end
