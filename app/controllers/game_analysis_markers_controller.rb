class GameAnalysisMarkersController < ApplicationController
  before_action :set_game_analysis_marker, only: [:show, :edit, :update, :destroy]

  # GET /game_analysis_markers
  # GET /game_analysis_markers.json
  def index
    @game_analysis_markers = GameAnalysisMarker.all
  end

  # GET /game_analysis_markers/1
  # GET /game_analysis_markers/1.json
  def show
  end

  # GET /game_analysis_markers/new
  def new
    @game_analysis_marker = GameAnalysisMarker.new
  end

  # GET /game_analysis_markers/1/edit
  def edit
  end

  # POST /game_analysis_markers
  # POST /game_analysis_markers.json
  def create
    @game_analysis_marker = GameAnalysisMarker.new(game_analysis_marker_params)

    respond_to do |format|
      if @game_analysis_marker.save
        format.html { redirect_to @game_analysis_marker, notice: 'Game analysis marker was successfully created.' }
        format.json { render :show, status: :created, location: @game_analysis_marker }
      else
        format.html { render :new }
        format.json { render json: @game_analysis_marker.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /game_analysis_markers/1
  # PATCH/PUT /game_analysis_markers/1.json
  def update
    respond_to do |format|
      if @game_analysis_marker.update(game_analysis_marker_params)
        format.html { redirect_to @game_analysis_marker, notice: 'Game analysis marker was successfully updated.' }
        format.json { render :show, status: :ok, location: @game_analysis_marker }
      else
        format.html { render :edit }
        format.json { render json: @game_analysis_marker.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /game_analysis_markers/1
  # DELETE /game_analysis_markers/1.json
  def destroy
    @game_analysis_marker.destroy
    respond_to do |format|
      format.html { redirect_to game_analysis_markers_url, notice: 'Game analysis marker was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_game_analysis_marker
      @game_analysis_marker = GameAnalysisMarker.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def game_analysis_marker_params
      params.require(:game_analysis_marker).permit(:game_analysis_id, :frame_from, :frame_to, :comment, :marker_type_id, :image)
    end
end
