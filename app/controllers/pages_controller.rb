class PagesController < ApplicationController
  def show
    render "pages/#{params[:id]}"
  end

  def about
    @user = User.new
    render "pages/about"
  end

  def dashboard
    @games = Game.includes(:users, :courses, :teams)
    render "pages/dashboard"
  end

  def contact
    @contact_form = ContactForm.new
    render "pages/contact"
  end

  def subscribe
    @user = User.new(user_params)
    @user.password = "EsportField2019"
    @user.password_confirmation = "EsportField2019"

    respond_to do |format|
      if @user.save
        UserMailer.signup_confirmation(@user).deliver
        format.html { redirect_to about_path, notice: 'You have subscribed successfully.' }
        format.json { render :show, status: :created, location: users_custom_path(@user) }
      else
        format.html { render :about }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def send_message
    @contact_form = ContactForm.new(contact_form_params)
    @contact_form.request = request

    respond_to do |format|
      if @contact_form.deliver
        format.html { redirect_to contact_path, notice: 'Your message has been sent successfully.' }
        format.json { render :show, status: :created, location: users_custom_path(@contact_form) }
      else
        format.html { render :contact }
        format.json { render json: @contact_form.errors, status: :unprocessable_entity }
      end
    end
  end

  private
		# Never trust parameters from the scary internet, only allow the white list through.
		def user_params
			params.require(:user).permit(:nickname, :email, :password)
		end
		def contact_form_params
			params.require(:contact_form).permit(:nickname, :email, :name, :message)
		end
end
