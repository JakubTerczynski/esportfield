class GameAnalysesController < ApplicationController
  before_action :set_game_analysis, only: [:show, :edit, :update, :destroy]

  # GET /game_analyses
  # GET /game_analyses.json
  def index
    @game_analyses = GameAnalysis.all
  end

  # GET /game_analyses/1
  # GET /game_analyses/1.json
  def show
  end

  # GET /game_analyses/new
  def new
    @game_analysis = GameAnalysis.new
  end

  # GET /game_analyses/1/edit
  def edit
  end

  # POST /game_analyses
  # POST /game_analyses.json
  def create
    @game_analysis = GameAnalysis.new(game_analysis_params)

    respond_to do |format|
      if @game_analysis.save
        format.html { redirect_to @game_analysis, notice: 'Game analysis was successfully created.' }
        format.json { render :show, status: :created, location: @game_analysis }
      else
        format.html { render :new }
        format.json { render json: @game_analysis.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /game_analyses/1
  # PATCH/PUT /game_analyses/1.json
  def update
    respond_to do |format|
      if @game_analysis.update(game_analysis_params)
        format.html { redirect_to @game_analysis, notice: 'Game analysis was successfully updated.' }
        format.json { render :show, status: :ok, location: @game_analysis }
      else
        format.html { render :edit }
        format.json { render json: @game_analysis.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /game_analyses/1
  # DELETE /game_analyses/1.json
  def destroy
    @game_analysis.destroy
    respond_to do |format|
      format.html { redirect_to game_analyses_url, notice: 'Game analysis was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_game_analysis
      @game_analysis = GameAnalysis.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def game_analysis_params
      params.require(:game_analysis).permit(:game_video_id, :summary, :recommendation, :coach_id)
    end
end
