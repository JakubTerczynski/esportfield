class User < ApplicationRecord
  acts_as_token_authenticatable
  mount_uploader :avatar, ImageUploader

  has_many :teams
  has_many :courses
  belongs_to :user_role
  belongs_to :game

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  def is_app_admin?
    user_role.level == 0 ? true : false
  end

  def is_founder?
    user_role.level == 1 ? true : false
  end

  def is_coach?
    user_role.level == 2 ? true : false
  end

  def is_player?
    user_role.level == 3 ? true : false
  end

  def to_s
    self.nickname
  end
end
