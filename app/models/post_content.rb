class PostContent < ApplicationRecord
  belongs_to :post
  mount_uploader :image_content, ImageUploader

  validates :image_content, presence: true
end
