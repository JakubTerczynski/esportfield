class MarkerType < ApplicationRecord
  has_many :game_analysis_markers

  def to_s
    self.name
  end
end
