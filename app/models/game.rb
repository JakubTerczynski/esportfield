class Game < ApplicationRecord
  mount_uploader :logo, ImageUploader

  has_many :users
  has_many :teams
  has_many :courses
  has_many :game_videos

  def to_s
    self.name
  end
end
