class Team < ApplicationRecord
  mount_uploader :logo, ImageUploader

  belongs_to :founder, class_name: "User"
  belongs_to :game

  def to_s
    self.name
  end
end
