class GameAnalysisMarker < ApplicationRecord
  mount_uploader :image, ImageUploader

  belongs_to :game_analysis
  belongs_to :marker_type, optional: true

  def to_s
    self.game_analysis.game_video.game
  end
end
