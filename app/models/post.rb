class Post < ApplicationRecord
  has_many :post_contents, inverse_of: :post
  accepts_nested_attributes_for :post_contents, reject_if: :all_blank, allow_destroy: true

  mount_uploader :image, ImageUploader
end
