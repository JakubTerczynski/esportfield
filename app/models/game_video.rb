class GameVideo < ApplicationRecord
  mount_uploader :video, VideoUploader

  has_many :game_analyses
  belongs_to :game
  belongs_to :player, class_name: "User"

  def to_s
    self.game.name
  end
end
