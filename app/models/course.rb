class Course < ApplicationRecord
  belongs_to :coach, class_name: "User"
  belongs_to :game

  def to_s
    self.game
  end
end
