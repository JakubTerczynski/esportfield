class GameAnalysis < ApplicationRecord
  belongs_to :game_video
  belongs_to :coach, class_name: "User"
  has_many :game_analysis_markers

  def to_s
    self.game_video.game
  end
end
