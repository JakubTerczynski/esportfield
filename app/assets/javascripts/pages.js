$(document).on('turbolinks:load', function() {
  AlertFade();

  function AlertFade() {
    setTimeout(function() {
      $(".alert").alert('close');
    }, 4000);
  }
});