document.addEventListener("turbolinks:load", function() {
  $("#file_image_input").filestyle({
    btnClass: "btn btn-default btn-round",
    input: false,
    text: "Add Image"
  })
})
